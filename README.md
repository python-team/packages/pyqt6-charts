# PyQt6-Charts - Python Bindings for the Qt Charts Library

PyQt6-Charts is a set of Python bindings for The Qt Company's Qt Charts
library.  The bindings sit on top of PyQt6 and are implemented as a single
module.


## Author

PyQt6-Charts is copyright (c) Riverbank Computing Limited.  Its homepage is
https://www.riverbankcomputing.com/software/pyqtchart/.

Support may be obtained from the PyQt mailing list at
https://www.riverbankcomputing.com/mailman/listinfo/pyqt/.


## License

PyQt6-Charts is released under the GPL v3 license and under a commercial
license that allows for the development of proprietary applications.


## Documentation

The documentation for the latest release can be found
[here](https://www.riverbankcomputing.com/static/Docs/PyQt6/).


## Installation

The GPL version of PyQt6-Charts can be installed from PyPI:

    pip install PyQt6-Charts

`pip` will also build and install the bindings from the sdist package but Qt's
`qmake` tool must be on `PATH`.

The `sip-install` tool will also install the bindings from the sdist package
but will allow you to configure many aspects of the installation.
